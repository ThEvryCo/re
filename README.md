# The Everything Co

Our goal is to create a business model which will allow for the sustainable development of Open Source projects.
The organization The Everything Co will be the first such instance of this model, with the goal of creating an open source alternative to many of the products that people on a daily basis.

#### This Repo (Core)
The core repository will contain all of the literature which outlines the functioning of such open source business models at the higher levels of abstraction, natural language.
Feedback, ideas, and criticisms are all welcome, please start an issue and a discussion around it.
If you wish to add an article or something which adds to the descriptive power of this repo pull request.
We do ask that you they are nicely formatted with markdown one sentence per line as to allow clean diffs when patching.

## Outline of Operation


#### Comiico: Open Source Cooperative Companies(do we need a title other than the everything co?)
### Work in Progress, forgive grammar and spelling just trying to ideas on paper, PRs welcome

***Abstract:***
The advent of blockchain technologies have given us new tools in the game theoretician's to build more robust incentive systems which will gives more flexibility to the game designer.
We propose a proof of authority/reputation system built on top of the #D protocol in which identities(on #D) issue tokens for work done.
These tokens represent a share of contribution to a specific set of information, these sets will generally be contained in a bounty in which the specified information meets a set of requirements.
The issuing identity should act as a buyer of last resort ensuring an exchange rate for tokens which de-risks contribution by developers by guaranteeing a minimum level of compensation.
The long term value of these tokens is in the donations received to the open source project.
When coupled with a modified copyleft license that adds strict attribution requirements it is trivial for donations to be dived up fairly amongst contributors, lowering the exit cost from a project since contributions are track on an individual level.
The other advantages of this model is that forks are cheap because contributions before forks can be rewarded from both forks.
When this model is combined with non-profits and other non-taxable vehicles it could lead to cost advantages even before considering the efficiencies of cooperation vs competition.



### Introduction
Building scalable organizations is a challenge that is yet on solved.
Most of the challenge comes in hiring, coordination, long term planning, and cost calculation.
Much of the cost associated with products that we buy are contributable to the organizational overhead.
This becomes less true the closer you get to producing the raw materials that go into the actual product, these cost are more associated with the energy required to physically produce and distribute.

The problem with our current organizational structures is that they are not scale free networks, as the size of the organization grows, there is a greater variety of relationships needed to function efficiently, the shape of the graph changes.
This is due to the variety of "plumbing" operations required for an organization to maintain itself through time.
Eventually HR, management, leadership, and accounting become their own sub-organizations whose incentives may not align.

While an organization such as a company may have many products when it grows to sufficient size its sub-organizations are creating internal products to maintain the organization.
For example HR's product is employees that fit with the company and are competent at generating value in their products that they sell externally.
Each of these forces are only extant to serve the goal of homeostasis of the organization, ensuring that the capital and human vehicle that is the organization is perpetuated.
This is extra work, just as the rest of the universe, reversing entropy is a costly endeavor that takes more energy than disorder.

The approach outlined in the following pages will expound upon the possibilities of having loosely coupled individuals that still get advantages granted by larger organizations while saving the extra work required to maintain a larger organization.

### Terms
##### Intellectual Property(IP)
We will use the abbreviation IP throughout this paper.
Our usage of the term doesn't quite match what most people think of with that term since open source doesn't really have exclusive ownership.
In this context we will be using the term to denote the valuable information generated in the pursuit of a goal.


##### Identities
This paper will not cover the specifics of implementation for the identities needed for this system to work.
The tentative plan is to finish development of the #D protocol as it meets the requirements needed for this system and is free and simple to work with.
The requirements for the system to work are identities which are uniquely identifiable, long lived, immutable, authenticated, secure(sybil resistant), consensus of time ordering of events, and support the issuance and transfer of unique traceable tokens.
When referring to these identities in this paper please bare in mind that identities could be a collective, individual, or synthetic entity operated by a smart contract, the important part is that they are a digitally authenticated entity.

##### Products
Product is general broad term to cover which has value outside of context of the project itself, i.e. marketable to others than participants in the project.
It is something that will be primarily used by a consumer and not a contributor but isn't restricted to such.
Products maybe incorporated in higher level products.


##### Product Tokens
A token which is likely to be seen primarily by consumers.
These tokens represent a share of contribution to the IP of a product, many of which are issued as a multiple of the IP's composing bounty tokens.  

##### Bounties
This is a token issuance trigger by the completion of a set of requirements.
Most will be intermediate bounties which on their own aren't very valuable to anyone outside of the project.
Some will be for product level requirement completion in which the result is something that is potentially marketable to a wider audience.

##### Bounty Tokens   
A Bounty Token is a token issued for completing an intermediate bounties whose requirements aren't useful to very many people outside of the project.
These tokens will generally be used to determine the amount of product tokens contributors will be rewarded with, regardless of who issues them.


### Hierarchy
Hierarchies are efficient structures, they were first evolved hundreds of millions of years ago as a response to the pressure associated with lack of resources.
They are efficient ways of distributing information and resources.
The goal of this project is to reduce the cost of forking and changing this hierarchy.
The low cost of exit keep leaders honest because they are only dictators if they are benevolent, if they aren't contributors will 'elect' a new dictator.
One identity may fulfill multiple roles at different level of the hierarchy simultaneously.
The described roles are more for illustration purposes because of their interchangeability.


##### Buyer of Last Resort(BLR)
At the top of the hierarchy is the backing identity which acts as the investor in the creation of the product.
While at the 'top' their only power comes from insuring a minimal level of compensation.  
This role is an optional role, anyone can issue tokens with no buyer of last resort.
However, without a Buyer of Last Resort this increases the risk for early contributors since they could be working for tokens that for a project that never is complete or end up being worthless.
Generally the BLR will be the product owner or an organization that the product owner belongs to.
If the BLR and product owner are not the same identity then there has to be some trust the BLR has in the product owner to issue tokens fairly/responsibly since the BLR will be buying them.
If a product owner deviates from the BLR's goals the BLR can backstop the tokens at a lower rate or stop supporting it entirely.
This won't stop the project if contributors still believe in the long term value of the project, it just adds risk to without having a minimum compensation level that BLR provides.  


##### Visionary/Product Owner
To pull new information out of the ether and into the realm of the known it generally takes someone or group which has the goal most clearly in their mind whose clarity lends speed to decision making.
The iPhone wasn't made by committee.
The product owners will be the ones who set the requirements and make the high level design decisions.
They will also be the ones deciding the number of tokens to issue for each bounty.
They also will be responsible for deciding the number of product tokens issued and what the conversion rate of the comprising bounty tokens to product tokens.


##### Contributors
These are the doers of the project, they contend with the devils in the details.
This the lowest position in the hierarchy when it comes to power of the direction of a product.
They can's influence the incentives or set requirements, their only power is through discussion with those who can or to exit and fork the project.



### Theory of Operation
Now that we have our terms and roles define we can start to explain how the sausage gets made.

We start as all things start, as an idea.
The visionary, starts by turning his vision into words or whatever media works to get the idea across.
From there its up to the visionary to create a concrete set of bounties which have deliverable requirement as to make it clear the expectations for contributors.
While all the Bounties don't need to be created at once it can give people a wider view of the projects architecture which might help attract more contributors.
Bounties should be sized to the largest scope in which the requirements are unlikely to change.
This size is dependent on the endeavor undertaken and the knowledge of the visionary.
Smaller bounties allows the project to evolve without having to recalculate the reward for larger bounties whose requirements are more likely to change.
One more benefit is that smaller bounties make it easier for projects to fork since tokens are being issued to smaller subsystems, it incentivizes the development of more reusable chunks.
When the functionality of the IP has been developed to the point where the project has more value as a whole than its parts, i.e. product release.


The product owner will issue product tokens, usually some of these issued tokens will be part of the product bounty which contains requirements that are bounties and probably some final steps to integrate the previously done work.
The majority of the tokens will likely be issued through bounty token conversion, where the product owner issues product tokens at a multiple of the bounty tokens for the bounties contained in the product bounty.
This multiple comes down to what the product owner decides the approximate value of the product at the completion of the bounty.
There isn't an objective way to come this, the incentive to be as close to the truth as possible is that their reputation for being fair is on the line, no one wants to work for unfair 'employers'.
This is further compounded by the fact that the contributors could effectively mutiny on the product release and issue their own product tokens at a fairer rate, however they won't have the buyer of last resort to back stop the market failure risk.

### Tokens

The tokens issued for each bounty will be unique and therefore will have almost no marketable value.
The same can be said for product tokens there are hopefully many more people interacting with your product than the individual bounties that were required to build the product.
The tokens really represent proof of contribution to the information contained in a product.
When coupled with a strict attribution license which requires the inclusion of a link to a blockchain from which anyone can determine the current rightful token holders.
Donations to the open source project can be given directly to the contributors/token holders.

##### Marketplace

By tokenizing the contributions it allows for outside investment to easily come and go from a project.
To allow this fluidity there will need to be an accessible and easy to use marketplace, any friction here reduces the potential investors and contributors.
To de-risk the market for the contributors the market place must support public programmable market orders.
For example the Buyer of Last Resort(BLR) could place a buy order for the entire supply of issued tokens minus token that have moved but are not sold to this order, which never expires, only be filled by coins that haven't moved(coming from the original contributor), and has a minimum price.
This conditional market order would de-risk work done by the contributors while minimizing the possibility of back stopping speculators while still allowing the BLR to compete for token by increasing the bid.
If contributors hold onto the tokens issued for bounties they participate in then it is implied that the potential value is greater than the BLR's price.
Outside organizations who want to incentivize the creation a project can simply add their own buy orders which might carry time restrictions so they can't attract lots of talent only to pull the order upon bounty completion leaving the product in open source while destroying the price that the contributors were expecting to get.
Some research needs to be done on the types of market that will get the desired results, fair to the participants relative to the risk they assume, possibly doing batched auctions for tokens instead of real time markets to alleviate front running or other advantages granted by the dynamics of the token issuance on distributed systems.


##### Forking
One of advantages to the tokenization of discrete chunks of contribution is their composability and reusability.
When a project forks the comprising intermediate bounty tokens could be reused as the basis of issuing the new forked product tokens.
Having forking as a first class citizen so to speak means there doesn't need to be a political/holy war when there is a fork in the project.
Even if it forks the core contributors will get their slice when any fork gets a donation.
The only power the new fork product owner has besides setting new bounties and the intermediate bounty token conversion rate to the new product tokens.

````
R1 R2 R1 R2 R1 R2 R1 R2 R3   R1 R2
| /    \/    \ /    \| /      \/
B1     B2     B3     B4       B5
  \     |\    /\   / |       /
   \    | \__/  \_/  |      /
    \   x2   /\ /\   |     /
     \x2|x2/x3/\  \  x4   /
      Prod1     \x3\x3 x2/
                   forkP2    
````
The above is a diagramed example of how this could work.
Each bounty has n requirements, Rx.
Each product is comprised of n bounties, Bx.
This example has the original product issuing multiples for it's comprising bounties, B1-B4.
Product 2 is a fork of the original product comprising of bounties B2-B5
This could be an example an app whose back-end stays the same but the ui is different.
The relative token issuance doesn't matter since the total issued is a representation of the total value.
The different multiples determine different bounties' share of the product tokens issued. 

##### Reputation
Having forks which effect the potential distribution of contribution tokens introduces some attack vectors that bad actors can exploit.
To mitigate such attacks having a contextual reputation to determine if you should trust a particular product token issuer.
An example of an attack is where an identity forks an IP base, does a trivial change to the IP while changing the product token issuance in away which their is a unfair token distribution, i.e. issuing a bunch of product tokens to themselves and not offering a multiple to the contained intermediate bounty tokens.

Having contributors hold tokens gives them the ability to make statements which concern their contributions.
The intermediate bounty token holders can use the proof that they have those tokens as license to make reputation attestation about a product owner.
Given a malicious forker the bounty token holders could make a simple attestation saying that a product owner is not acting in good faith.
This also works for product owners who miss the mark when it comes to token conversion rate, instead of giving them a bad wrap they could vote to give feedback to the product owner about what they think about the pay schedule.
This gives a consensus mechanism to the process that allows better results over time while not forcing each token issuance to vote on a possibly contentious topic.
Delegating this responsibility to he person with the most visibility into it, the product owner, alleviating managerial cognitive load on contributors that just want to create.


##### License
The linchpin that protects the project from just being copied without attribution would be a modified copyleft which requires strict attribution which makes the discovery of current token holders standardized and as low friction as possible.
Licenses can cause various compatibility issues when coupling code with different licenses, any permissive license should be compatible.
More research and discussion is needed to find the optimal solution, it may just be an additional attribution clause that can allow for contributor token model to work even if it isn't under a strict copyleft license.
This approach would still have issue due to companies not wanting to have the extra friction of accommodating a new specialized attribution that likely won't be able to be dropped in as other existing attribution licenses.

##### Hour Tokens(not 100% sure on this)
Another token can be issued by the product owner in which they verify the hours spent on working on the project.
This can be another way to add value for the contributors as it is a way to build reputation.
These can be more thought of attestations since they can't be traded, or at least it wouldn't make sense to.
If they are tokenized in the sense that they are each uniquely identifiable you could also use these tokens for project voting mechanisms to defend against those with money having undue influence on the project.
This is about as equitable as you can get, we all get the same number of hours each day, those that spend that scarce resource on the project should have their time respected.
